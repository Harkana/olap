exports.install = function() {
	F.route('/', view_index);
	F.route('/parse/{sourceType}/uniqueResult', parseUniqueResult, ["post"])
    F.route('/parse/{sourceType}/nonUniqueResult', parseNonUniqueResult, ["post"])
	// or
	// F.route('/');
};

function view_index() {
	var self = this;
	self.view('index');
}

function parseUniqueResult(sourceType){
	let body = this.body

	MODEL('parser').getSourceTypes(sourceType).parseUniqueResult(body, (item) => {
		this.json(item)
	}, (message) => {
		this.throw400(message)
	})
}

function parseNonUniqueResult(sourceType){
	let body = this.body

    MODEL('parser').getSourceTypes(sourceType).parseNonUniqueResult(body, (list) => {
		this.json(list)
    }, (message) => {
    	this.throw400(message)
	})
}
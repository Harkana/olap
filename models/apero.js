'use strict'
/**
 * Created by arkanzel on 10/12/16.
 */

const modelsApero = require('./utils/apero/apero_utils')

class Apero
{
    parseUniqueResult(body, success, error){
        let ret_json = {}

        if (!body.url){
            error("url undefined")
            return;
        }
        modelsApero.getName(body.url, (name) => {
            modelsApero.getCodePostal(body.url, (codePostal) => {
                ret_json['name'] = name
                ret_json['code_postal'] = codePostal
                ret_json['source'] = CONFIG("LES_AMIS_DE_LAPERO")
                success(ret_json)
            })
        })
    }

    parseNonUniqueResult(request, success, error){
        if (!request.body || !request.url){
            error('body or url undefined')
            return;
        }

        let body = request.body
        let url = request.url

        modelsApero.getListName(url, body, (list) => {
            if (list == undefined){
                list = []
                error('list undefined')
                return;
            }
            success({
                records : list,
                total : list.length
            })
        })
    }
}

module.exports = Apero
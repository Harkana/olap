const http = require('http')
const querystring = require('querystring');

let utils = {
    getName : (url, ft) => {
        let indexBegin = url.lastIndexOf("/")
        let name = ""
        let i = indexBegin + 1
        while(url.charAt(i).search(/[0-9]/) != -1){
            i++
        }
        for (;i < url.length;i++){
            let flag = false
            while(url.charAt(i).search(/[0-9a-z]/i) == -1){
                i++
                flag = true
            }
            if (flag){
                name += name.length ? " " : ""
                name += url.charAt(i).toUpperCase()
                flag = false
            }else{
                name += name.length ? url.charAt(i) : url.charAt(i).toUpperCase()
            }
        }
        ft(name)
    },
    getCodePostal : (url, ft) => {
        let indexBegin = url.indexOf("w")
        let indexInter = url.indexOf('/', indexBegin)
        let indexEnd = url.length
        let host = url.slice(indexBegin, indexInter)
        let path = url.slice(indexInter, indexEnd)

        http.get({
            hostname:host,
            port: 80,
            path: path,
        }, (response) => {
            let data = ""

            response.on('data', (chunck) => {
                data += chunck
            })

            response.on('end', () => {
                if (data) {
                    let ret = data.match(/<a href=".*arrondissement.*">([0-9]+).*<\/a>/)
                    let codePostal = 75000 + Number(ret[1])
                    ft(codePostal)
                }
            })
        })
    },
    getListName : (url, body, ft) => {
        let indexBegin = url.indexOf("w")
        let indexInter = url.indexOf('/', indexBegin)
        let indexEnd = url.length
        let host = url.slice(indexBegin, indexInter)
        let path = url.slice(indexInter, indexEnd)
        let nb1 = parseInt(body.cp)
        let params = {
            'arrondissement': (parseInt(body.cp) % 75000)
        }
        let postData = "nom_bar=un+nom+de+bar+%3F&"

        postData += querystring.stringify(params)
        let options = {
            method: "POST",
            hostname: host,
            path: path,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': Buffer.byteLength(postData)
            }
        }

        let request = http.request(options, (res) => {
            let data = ""
            res.setEncoding('utf8');

            res.on('data', (chunck) => {
                data += chunck
            })

            res.on('end', () => {
                let ret = data.match(/<a href="\/paris\/bars\/.*">(.*)<\/a>/g)
                let newList = ret.map((item) => {
                    let indexBegin = item.indexOf("/") + 1
                    let indexEnd = item.indexOf(">") - 1
                    let url = item.slice(indexBegin, indexEnd)
                    let i = url.lastIndexOf("/") + 1
                    url = url.slice(i, url.length)
                    i = 0
                    let name = ""

                    while (url.charAt(i).search(/[0-9]/) != -1 && i < url.length) {
                        i++
                    }
                    for (; i < url.length; i++) {
                        let flag = false
                        while (url.charAt(i).search(/[0-9a-z]/i) == -1 && i < url.length) {
                            i++
                            flag = true
                        }
                        if (flag) {
                            name += name.length ? " " : ""
                            name += url.charAt(i).toUpperCase()
                            flag = false
                        } else {
                            name += name.length ? url.charAt(i) : url.charAt(i).toUpperCase()
                        }
                    }
                    return (name)
                })
                ft(newList)
            })
        })
        request.write(postData)
        request.end()
    }
}

module.exports = utils



exports.id = "parser"

const sourceTypes = {
    'LES_AMIS_DE_LAPERO' : require('./apero'),
    'VILLA_SCHWEPPES' : '',
    'GUSTAVE_ET_ROSALIE' : ''
}

exports.getSourceTypes = (name) => {
    return (new sourceTypes[name]())
}
